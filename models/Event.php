<?php
namespace app\models;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 12:55 PM
 */
class Event
{


    public $task_id;
    public $user_name;
    public $email;
    public $task;
    public $images;
    public $status;
    public $errors;

    public function __construct()
    {
        $this->task_id = null;
        $this->user_name = '';
        $this->email = '';
        $this->task = '';
        $this->status = Status::$NEW_STATUS;
        $this->images = [];
        $this->errors = [];
    }


    public function load($values)
    {
        if (empty($values)) {
            return false;
        } else {
            $this->setAttributes($values);
            return true;
        }
    }

    public function setAttributes($values, $safe_attributes = true)
    {
        if (isset($values['task_id']) && !$safe_attributes) {
            $this->task_id = $values['task_id'];
        }
        foreach ($values as $field_name => $field_value) {
            if (isset($this->{$field_name}) && (!$safe_attributes || ($safe_attributes && in_array($field_name, $this->getSafeAttributes())))) {
                $this->{$field_name} = $field_value;
            }
        }
    }


    public function getAttributeLabel($attribute)
    {
        $attributes_names = [
            'user_name' => 'Имя пользователя',
            'email' => 'Email',
            'task' => 'Текст задачи',
        ];
        return isset($attributes_names[$attribute]) ? $attributes_names[$attribute] : $attribute;
    }

    public function getSafeAttributes()
    {
        return [
            'user_name',
            'email',
            'task',
        ];
    }

    public function getSafeSearchAttributes()
    {
        return [
            'user_name',
            'email',
            'task',
            'status',
        ];
    }

    public function verifyModel()
    {
        $all_success = true;
        if (!preg_match("#^[a-zA-Z0-9\_\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z]+$#", $this->email, $match)) {
            $all_success = false;
            $this->errors['email'][] = 'Email указан не верно.';
        }
        if (empty($this->email)) {
            $all_success = false;
            $this->errors['email'][] = 'Необходимо указать поле Email.';
        }
        if (empty($this->user_name)) {
            $all_success = false;
            $this->errors['user_name'][] = 'Необходимо указать поле Имя пользователя.';
        }
        if (empty($this->task)) {
            $all_success = false;
            $this->errors['task'][] = 'Необходимо указать поле Текст задачи.';
        }
        return $all_success;
    }

    public function getErrors()
    {
        foreach ($this->errors as $errors) {
            return implode('<br>', $errors);
        }
        return '';
    }

    public function getError($field)
    {
        if (isset($this->errors[$field])) {
            return implode('<br>', $this->errors[$field]);
        }
        return '';
    }

    public function saveModel()
    {
        if ($this->task_id == null) {
            return $this->insertModel();
        }
        $prepeared_command = \App::$app->db->prepare("UPDATE `tasks` SET `user_name`=:user_name, `email`=:email, `task`=:task, `status`=:status WHERE `task_id`=:task_id ");
        $prepeared_command->execute([':user_name' => $this->user_name, ':email' => $this->email, ':task' => $this->task, ':status' => $this->status, ':task_id' => $this->task_id]);
        return true;
    }

    public static function getModel($model_id)
    {
        $prepeared_command = \App::$app->db->prepare("SELECT `task_id`, `user_name`, `email`, `task`, `status` FROM `tasks` WHERE `task_id`=:task_id");
        $prepeared_command->execute([':task_id' => $model_id]);
        if ($row = $prepeared_command->fetch()) {
            $model = new Event();
            $model->setAttributes($row, false);
            $images_prepeared_statement = \App::$app->db->prepare("SELECT `image_id`, `task_id`, `file_name` FROM `tasks_images` WHERE `task_id`=:task_id");
            $images_prepeared_statement->execute([':task_id' => $model_id]);
            $images = $images_prepeared_statement->fetchAll();
            foreach ($images as $image) {
                $model->addImage($image);
            }
            return $model;
        } else {
            return null;
        }
    }

    public function setStatus($new_status)
    {
        if (in_array($new_status, Status::getAllElements())) {
            $this->status = $new_status;
        }
    }

    public function insertModel()
    {
        $prepeared_command = \App::$app->db->prepare("INSERT INTO `tasks` (`user_name`, `email`, `task`, `status`) VALUES(:user_name, :email, :task, :status)");
        $prepeared_command->execute([':user_name' => $this->user_name, ':email' => $this->email, ':task' => $this->task, ':status' => $this->status]);
        $this->task_id = \App::$app->db->lastInsertId();
        return true;
    }

    public function addImage($image_array)
    {
        $image_model = new Image();
        $image_model->setAttributes($image_array, false);
        $this->images[] = $image_model;
    }

    public function getStatus()
    {
        return Status::getName($this->status);
    }

    public static function getAll($params)
    {
        $model = new Event();
        $model->setAttributes($params);
        $criteria = ['status' => " (`status`='" . Status::$CREATED_STATUS . "' OR `status`='" . Status::$DONE_STATUS . "') "];
        $orders = [];
        if (isset($params['order'])) {
            list($field_name, $order) = explode('-', $params['order']);
            if (in_array($field_name, $model->getSafeSearchAttributes())) {
                $orders[$field_name] = "`$field_name` " . ($order == 'DESC' ? 'DESC' : 'ASC');
            }
        }
        if (count($orders) == 0) {
            $orders = [" `task_id` DESC "];
        }

        $offset = 0;
        if (isset($params['page'])) {
            $page = (int)$params['page'];
            if ($page > 0) {
                $page -= 1;
            } else {
                $page = 0;
            }
            $offset = $page * 3;
        }
        $prepeared_command = \App::$app->db->prepare("SELECT COUNT(*) as `total_count` FROM `tasks` WHERE " . implode(' AND ', $criteria) . " ");
        $prepeared_command->execute();
        $total_count = $prepeared_command->fetchColumn();
        $models = [];
        if ($total_count > 0) {
            $prepeared_command = \App::$app->db->prepare("SELECT `task_id`, `user_name`, `email`, `task`, `status` FROM `tasks` WHERE " . implode(' AND ', $criteria) . " ORDER BY  " . implode(', ', $orders) . " LIMIT $offset,3");
            $prepeared_command->execute();

            $rows = $prepeared_command->fetchAll();
            $loaded_tasks_ids = [];
            foreach ($rows as $row) {
                $loaded_tasks_ids[$row['task_id']] = $row['task_id'];
                $set_model = new Event();
                $set_model->setAttributes($row, false);
                $models[$row['task_id']] = $set_model;
            }
            if (count($loaded_tasks_ids) > 0) {
                $images_prepeared_statement = \App::$app->db->prepare("SELECT `image_id`, `task_id`, `file_name` FROM `tasks_images` WHERE `task_id` IN (" . implode(', ', $loaded_tasks_ids) . ")");
                $images_prepeared_statement->execute();
                $images = $images_prepeared_statement->fetchAll();
                foreach ($images as $image) {
                    $models[$image['task_id']]->addImage($image);
                }
            }
        }
        return ['total_count' => $total_count, 'models' => $models];
    }

}