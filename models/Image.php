<?php
namespace app\models;
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 12:55 PM
 */
class Image
{


    public $image_id;
    public $task_id;
    public $file_name;
    public $errors;

    public function __construct()
    {
        $this->image_id = null;
        $this->task_id = '';
        $this->file_name = '';
        $this->errors = [];
    }


    public function load($values)
    {
        if (empty($values)) {
            return false;
        } else {
            $this->setAttributes($values);
            return true;
        }
    }

    public function setAttributes($values, $safe_attributes = true)
    {
        if (isset($values['image_id']) && !$safe_attributes) {
            $this->task_id = $values['image_id'];
        }
        foreach ($values as $field_name => $field_value) {
            if (isset($this->{$field_name}) && (!$safe_attributes || ($safe_attributes && in_array($field_name, $this->getSafeAttributes())))) {
                $this->{$field_name} = $field_value;
            }
        }
    }

    public function getWebPath()
    {
        return "/images/$this->task_id/$this->file_name";
    }


    public function getAttributeLabel($attribute)
    {
        $attributes_names = [
            'file_name' => 'Название файла',
            'task_id' => 'Задача',

        ];
        return isset($attributes_names[$attribute]) ? $attributes_names[$attribute] : $attribute;
    }

    public function getSafeAttributes()
    {
        return [
            'file_name',
            'task_id',
        ];
    }


    public function verifyModel()
    {
        return true;
    }

    public function getErrors()
    {
        foreach ($this->errors as $errors) {
            return implode('<br>', $errors);
        }
        return '';
    }

    public function getError($field)
    {
        if (isset($this->errors[$field])) {
            return implode('<br>', $this->errors[$field]);
        }
        return '';
    }

    public function saveModel()
    {
        if ($this->image_id == null) {
            return $this->insertModel();
        }
        $prepeared_command = \App::$app->db->prepare("UPDATE `tasks_images` SET `task_id`=:task_id, `file_name`=:file_name WHERE `image_id`=:image_id ");
        $prepeared_command->execute([':task_id' => $this->task_id, ':file_name' => $this->file_name, ':image_id' => $this->image_id]);
        return true;
    }

    public static function getModel($model_id)
    {
        $prepeared_command = \App::$app->db->prepare("SELECT `image_id`, `task_id`, `file_name` FROM `tasks_images` WHERE `image_id`=:image_id");
        $prepeared_command->execute([':image_id' => $model_id]);
        if ($row = $prepeared_command->fetch()) {
            $model = new Image();
            $model->setAttributes($row, false);
            return $model;
        } else {
            return null;
        }
    }

    public function insertModel()
    {
        $prepeared_command = \App::$app->db->prepare("INSERT INTO `tasks_images` (`task_id`, `file_name`) VALUES(:task_id, :file_name)");
        $prepeared_command->execute([':task_id' => $this->task_id, ':file_name' => $this->file_name,]);
        $this->image_id = \App::$app->db->lastInsertId();
        return true;
    }

    /**
     * @todo need to be added
     * @param $params
     * @return array
     */
    public static function getAll($params)
    {
        return [];
    }

}