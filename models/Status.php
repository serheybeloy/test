<?php

namespace app\models;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 1:07 PM
 */
class Status
{

    public static $NEW_STATUS = 10;
    public static $CREATED_STATUS = 15;
    public static $DONE_STATUS = 20;

    public static function getAllElements()
    {
        return [self::$CREATED_STATUS, self::$DONE_STATUS];
    }

    public static function getName($status)
    {
        $attributes = [
            self::$CREATED_STATUS => 'Активана',
            self::$DONE_STATUS => 'Завершена',
        ];
        if (isset($attributes[$status])) {
            return $attributes[$status];
        }
        return '';
    }

}