<?php

namespace app\controllers;


use app\components\ImageCropper;
use app\models\Event;
use app\models\Image;
use app\models\Status;
use MongoDB\Driver\Exception\ExecutionTimeoutException;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 12:17 PM
 */
class SiteController extends BaseController
{

    public function actionIndex()
    {
        $data = Event::getAll($_GET);
        echo $this->render('/site/index', ['data' => $data]);
    }

    public function actionCreateTask()
    {
        $model = new Event();
        if (isset($_POST['task_id'])) {
            $model = $this->loadModel($_POST['task_id']);
            if ($model->load($_POST) && $model->verifyModel()) {
                $model->setStatus(Status::$CREATED_STATUS);
                $model->saveModel();
                $this->redirect(['site/index']);
            }
        } else {
            $model->saveModel();
        }
        echo $this->render('/site/create_task', ['model' => $model]);
    }

    public function actionUploadImage()
    {
        $output = [];
        $success = false;
        if (isset($_POST['task_id']) && isset($_FILES['images'])) {
            $task_id = $_POST['task_id'];
            $source = $_FILES['images']['tmp_name'];
            $dest_dir = \App::getAlias('@app/web/images/' . $task_id);
            if (!file_exists($dest_dir)) {
                @mkdir($dest_dir, 0777, true);
            }
            $file_name = md5($_FILES['images']['name'] . time()) . '.' . pathinfo($_FILES['images']['name'], PATHINFO_EXTENSION);
            $dest = realpath($dest_dir) . '/' . $file_name;
            if (ImageCropper::makeThump($source, $dest, 320, 240)) {
                $image = [
                    'task_id' => $task_id,
                    'file_name' => $file_name,
                ];
                $image_model = new Image();
                $image_model->setAttributes($image);
                if ($image_model->saveModel()) {
                    $success = true;
                } else {
                    $output = ['error' => $image_model->getErrors()];
                }
            } else {
                $output = ['error' => "Ошибка изменения размера"];
            }
        }
        if (!$success) {
            if (!isset($output['error'])) {
                $output = ['error' => 'Ошибка при загрузке файла'];
            }
        }
        echo json_encode($output);
    }


    public function actionPreviewTask($id)
    {
        $model = $this->loadModel($id);
        $model->load($_POST);
        echo $this->renderPartial('/site/view', ['model' => $model]);
    }

    public function loadModel($id)
    {
        if (($model = Event::getModel($id)) == null) {
            throw new \Exception('Даная задача не найдена', 404);
        }
        return $model;


    }

}