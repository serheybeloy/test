<?php

namespace app\controllers;


use app\components\ImageCropper;
use app\models\Event;
use app\models\Image;
use app\models\Status;
use MongoDB\Driver\Exception\ExecutionTimeoutException;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 12:17 PM
 */
class AdminController extends BaseController
{

    public function actionIndex()
    {
        $data = Event::getAll($_GET);
        echo $this->render('/admin/index', ['data' => $data]);
    }

    public function actionUpdateTask($id)
    {
        $model = $this->loadModel($id);
        if ($model->load($_POST) && $model->verifyModel()) {
            if (isset($_POST['status'])) {
                $model->setStatus($_POST['status']);
            }
            $model->saveModel();
            $this->redirect(['admin/index']);
        }
        echo $this->render('/admin/update_task', ['model' => $model]);
    }

    public function loadModel($id)
    {
        if (($model = Event::getModel($id)) == null) {
            throw new \Exception('Даная задача не найдена', 404);
        }
        return $model;


    }

}