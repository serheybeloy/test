<?php

namespace app\controllers;

use app\components\Url;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 12:17 PM
 */
class BaseController
{

    public function redirect($url)
    {
        print_r(\App::$app->route);
        if (is_array($url)) {
            $url = \App::$app->route->scheme . '://' . \App::$app->route->domain . '' . Url::to($url);
        }
        header("Location: $url");
        exit();
    }

    public function __call($methodName, $args = array())
    {
        if (is_callable(array($this, $methodName)))
            return call_user_func_array(array($this, $methodName), $args);
        else
            throw new \Exception('In controller ' . get_called_class() . ' method ' . $methodName . ' not found!');
    }

    public function renderPartial($view, $values)
    {
        ob_start();
        ob_implicit_flush(false);
        extract($values, EXTR_OVERWRITE);
        require(\App::getAlias("@app/views$view.php"));
        return ob_get_clean();
    }

    public function render($view, $values)
    {
        $content = $this->renderPartial($view, $values);
        $params = ['content' => $content,];

        return $this->renderPartial('/layout/main', $params);
    }

}