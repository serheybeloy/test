<?php

?>

<div class="">
    <form method="POST" enctype="multipart/form-data" id="create-task-form">

        <div class="form-group field-task-task <?= !empty($model->getError('task')) ? 'has-error' : '' ?>">
            <label class="control-label" for="task-task"><?= $model->getAttributeLabel('task') ?></label>
            <textarea name="task" id="task-task" class="form-control"><?= $model->task ?></textarea>
            <div class="help-block"><?= $model->getError('task') ?></div>
        </div>
        <div class="form-group field-task-status <?= !empty($model->getError('status')) ? 'has-error' : '' ?>">
            <label class="control-label" for="task-statusa"><?= $model->getAttributeLabel('status') ?></label>
            <select name="status" id="task-status" class="form-control">
                <option value="15" <?= $model->status == '15' ? 'selected' : '' ?> >Активна</option>
                <option value="20" <?= $model->status == '20' ? 'selected' : '' ?> >Выполнена</option>
            </select>
            <div class="help-block"><?= $model->getError('status') ?></div>
        </div>

        <button class="btn btn-primary" type="submit">Сохранить</button>
    </form>
</div>

