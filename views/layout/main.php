<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <title>My Yii Application</title>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/fileinput.css" rel="stylesheet">
    <link href="/css/site.css" rel="stylesheet">
</head>
<body>

<div class="wrap">
    <nav id="w0" class="navbar-inverse  navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse"><span
                            class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span></button>
                <a class="navbar-brand" href="/index.php">Home</a></div>
            <div id="w0-collapse" class="collapse navbar-collapse">
                <ul id="w1" class="navbar-nav navbar-right nav">
                    <li><a href="<?= \app\components\Url::to(['site/index']) ?>">Список задач</a></li>
                    <li><a href="<?= \app\components\Url::to(['site/createTask']) ?>">Добавить новую задачу</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content container-fluid ">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
    </div>
</footer>
<div class="modal" id="mainModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть окно">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть окно</button>
            </div>
        </div>
    </div>
</div>

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/modal.js"></script>
<script src="/js/fileinput.js"></script>
<script src="/js/ru.js"></script>
<?php

$js = App::$app->getJsCode();
if (!empty($js)) {
    echo "<script type=\"application/javascript\">";
    echo $js;
    echo "</script>";
}
?>
</body>
</html>