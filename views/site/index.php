<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 5:18 PM
 */

?>
<div class="row">
    <div class="row">
        <form method="GET">
            <div class="form-group field-task-ordere  col-md-6 col-xs-10">
                <label class="control-label" for="task-order">Отсортировать список по:</label>
                <select class="form-control" name="order">
                    <option value="">
                        Не сортировать
                    </option>
                    <option value="user_name-ASC" <?= isset($_GET['order']) && $_GET['order'] == 'user_name-ASC' ? 'selected' : '' ?>>
                        Пользователь А-Я
                    </option>
                    <option value="user_name-DESC" <?= isset($_GET['order']) && $_GET['order'] == 'user_name-DESC' ? 'selected' : '' ?>>
                        Пользователь Я-А
                    </option>
                    <option value="email-ASC" <?= isset($_GET['order']) && $_GET['order'] == 'email-ASC' ? 'selected' : '' ?>>
                        Email А-Я
                    </option>
                    <option value="email-DESC" <?= isset($_GET['order']) && $_GET['order'] == 'email-DESC' ? 'selected' : '' ?>>
                        Email Я-А
                    </option>
                    <option value="status-ASC" <?= isset($_GET['order']) && $_GET['order'] == 'status-ASC' ? 'selected' : '' ?>>
                        По статусу (впереди невыполненные)
                    </option>
                    <option value="status-DESC"<?= isset($_GET['order']) && $_GET['order'] == 'status-Desc' ? 'selected' : '' ?>>
                        По статусу (впереди выполненные)
                    </option>
                </select>
            </div>
            <div class="form-group field-task-ordere  col-md-2 col-xs-10">
                <button class="btn btn-primary" type="submit">Отсортировать</button>
            </div>
        </form>
    </div>
    <div>
        <?php foreach ($data['models'] as $model) {
            ?>
            <div class="well col-xs-12 col-md-12 col-lg-12">
                <?= App::$app->controller->renderPartial('/site/view', ['model' => $model]) ?>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="text-center">
        <?php
        $get = $_GET;
        for ($i = 1; $i <= ceil($data['total_count'] / 3); $i++) {
            $get['page'] = $i;
            ?>
            <a href="<?= \app\components\Url::to(['site/index'] + $get) ?>"><?= $i ?></a>
        <?php } ?>
    </div>
</div>

