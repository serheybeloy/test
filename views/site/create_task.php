<?php
App::$app->addNewJs("
    $(\"#images-file-input\").fileinput({
        language:\"ru\",
        purifyHtml:true,
        fileActionSettings: { showRemove: false } ,
        initialPreview:false,
        maxFileCount: 10,
        uploadUrl: '" . \app\components\Url::to(['site/uploadImage']) . "',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        uploadExtraData: {
                        task_id: '$model->task_id',
                    },
        elErrorContainer: '#error-block'
    }).on(\"filebatchselected\", function(event, files) {
        $(\"#images-file-input\").fileinput(\"upload\");
    });;
    
    $('.preview-button').click(function(){
    console.log($( \"#create-task-form\" ).serialize());
      
        $.post(\"" . \app\components\Url::to(['site/previewTask', 'id' => $model->task_id]) . "\", $(\"#create-task-form\").serialize()).done(function(data) {
            $(\".modal-title\").html('Пердпросмотр задачи');
            $(\".modal-body\").html(data);
            $('#mainModal').modal();
          }); 
        return false;
    });
    
");

?>

<div class="">
    <form method="POST" enctype="multipart/form-data" id="create-task-form">
        <input type="hidden" name="task_id" value="<?= $model->task_id ?>">
        <div class="form-group field-task-user-name <?= !empty($model->getError('user_name')) ? 'has-error' : '' ?>">
            <label class="control-label" for="task-user-name"><?= $model->getAttributeLabel('user_name') ?></label>
            <input type="text" id="task-user-name" class="form-control" name="user_name" maxlength="127"
                   value="<?= $model->user_name ?>" aria-invalid="false">
            <div class="help-block"><?= $model->getError('user_name') ?></div>
        </div>

        <div class="form-group field-task-email <?= !empty($model->getError('email')) ? 'has-error' : '' ?>">
            <label class="control-label" for="task-email"><?= $model->getAttributeLabel('email') ?></label>
            <input type="text" id="task-email" class="form-control" name="email" maxlength="127"
                   value="<?= $model->email ?>" aria-invalid="false">
            <div class="help-block"><?= $model->getError('email') ?></div>
        </div>

        <div class="form-group field-task-task <?= !empty($model->getError('task')) ? 'has-error' : '' ?>">
            <label class="control-label" for="task-task"><?= $model->getAttributeLabel('task') ?></label>
            <textarea name="task" id="task-task" class="form-control"><?= $model->task ?></textarea>
            <div class="help-block"><?= $model->getError('task') ?></div>
        </div>

        <div class="form-group">
            <div class="file-loading">
                <input type="file" id="images-file-input" name="images" class="form-control" multiple
                       placeholder='Выберите файл...'/>
                <button class="btn btn-default btn-choose" type="button">Выбрать файл</button>
            </div>
        </div>

        <div id="error-block"></div>

        <button class="btn btn-primary" type="submit">Создать</button>
        <button class="btn btn-primary preview-button">Предпросмотр</button>
    </form>
</div>
