<div class=" row task-view">
    <div class="col-xs-12 col-md-4">
        <div>
            <b>Имя пользователя:</b> <?= $model->user_name ?>
        </div>
        <div>
            <b>Email:</b> <?= $model->email ?>
        </div>
        <div>
            <b>Задача:</b> <?= $model->task ?>
        </div>
        <div>
            <b>Состояние задачи:</b> <?= $model->getStatus() ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-8">
        <?php
        foreach ($model->images as $image) {
            ?>
            <div class="task-image">
                <?php
                echo "<img src=\"" . $image->getWebPath() . "\" alt=\"\" >";
                ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>