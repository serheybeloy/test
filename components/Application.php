<?php

namespace app\components;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/28/17
 * Time: 3:51 PM
 */
class Application
{

    public $db = null;
    public $route = null;
    public $controller = null;


    protected $js_code = '';

    public function addNewJs($code)
    {
        $this->js_code .= $code;
    }

    public function getJsCode()
    {
        return $this->js_code;
    }

    public function __construct($config)
    {
        $this->db = new \PDO($config['db']['dns'], $config['db']['username'], $config['db']['password'], $config['db']['options']);
        $this->route = new Router();
    }

    public function start()
    {
        $this->route->parse($_SERVER['REQUEST_URI']);
        $controller_name = '\\app\\controllers\\' . ucfirst($this->route->controller) . 'Controller';
        $this->controller = new $controller_name();
        $this->controller->__call('action' . ucfirst($this->route->action), $this->route->request);
    }

}