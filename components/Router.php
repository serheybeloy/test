<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/29/17
 * Time: 12:16 AM
 */

namespace app\components;


class Router
{
    public $action = 'index';
    public $scheme = 'http';
    public $domain = '';
    public $controller = false;
    public $request = [];


    function parse($path)
    {
        $templates = [
            "([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([0-9]+)" => '$controller/$action/$id',
            "([a-z0-9+_\-]+)/([a-z0-9+_\-]+)" => '$controller/$action',
        ];
        $request = $_REQUEST;
        $request['controller'] = 'site';
        $request['action'] = 'index';
        $url_parts = parse_url($path);
        if (isset($url_parts['query']) and !empty($url_parts['query'])) {
            $path = str_replace('?' . $url_parts['query'], '', $path);
            parse_str($url_parts['query'], $req);
            $request = array_merge($request, $req);
        }
        foreach ($templates as $rule => $keypath) {
            if (preg_match('#' . $rule . '#sui', $path, $list)) {
                $path_elements = explode('/', $keypath);
                for ($i = 1; $i < count($list); $i = $i + 1) {
                    $keypath = preg_replace('#\$[a-z0-9]+#', $list[$i], $keypath, 1);
                }
                $keypath = explode('/', $keypath);
                foreach ($keypath as $i => $key) {
                    $request[str_replace('$', '', $path_elements[$i])] = $key;
                }
            }
        }
        if (isset($request['controller']))
            $this->controller = $request['controller'];
        if (isset($request['action'])) {
            $this->action = $request['action'];
        }
        if (isset($_SERVER['REQUEST_SCHEME'])) {
            $this->scheme = $_SERVER['REQUEST_SCHEME'];
        }
        if (isset($_SERVER['HTTP_HOST'])) {
            $this->domain = $_SERVER['HTTP_HOST'];
        }

        $this->request = $request;
        return $request;
    }
}