<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/29/17
 * Time: 1:23 AM
 */

namespace app\components;


class Url
{
    public static function to($params)
    {
        if (is_array($params)) {
            $first = array_shift($params);
            list($controller, $action) = explode('/', $first);
            $args = [];
            foreach ($params as $param_key => $param_value) {
                $args[$param_key] = urlencode($param_key) . "=" . urlencode($param_value);
            }
            return "/$controller/$action" . (count($args) > 0 ? '?' . implode('&', $args) : '');
        } else {
            return $params;
        }
    }
}