<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/29/17
 * Time: 1:44 AM
 */

namespace app\components;


use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Imagick\Image;

class ImageCropper
{
    public static function makeThump($source, $destination, $width, $height)
    {
        $imagine = new Imagine();
        $image = $imagine->open($source);
        $size = $image->getSize();
        if ($size->getWidth() > $width || $size->getHeight() > $height) {
            $width_k = $size->getWidth() / $width;
            $height_k = $size->getHeight() / $height;
            if ($width_k > $height_k) {
                $height = $size->getHeight() / $width_k;
            } else {
                $width = $size->getWidth() / $height_k;
            }
            $image->thumbnail(new Box($width, $height))->save($destination, ['quality' => 100]);
        }
        return true;
    }
}