<?php

require realpath(__DIR__ . '/..') . '/vendor/autoload.php';

class App
{
    public static $app;

    public static $aliases = [
        '@app' => __DIR__ . '/../',
        '@web' => __DIR__,
    ];

    public static function autoload($className)
    {
        if (strpos($className, '\\') !== false) {
            $classFile = static::getAlias('@' . str_replace('\\', '/', $className) . '.php', false);
            if ($classFile === false || !is_file($classFile)) {
                return;
            }
        } else {
            return;
        }
        include($classFile);
    }

    public static function getAlias($alias)
    {
        if (strncmp($alias, '@', 1)) {
            // not an alias
            return $alias;
        }
        $pos = strpos($alias, '/');
        $root = $pos === false ? $alias : substr($alias, 0, $pos);
        if (isset(static::$aliases[$root])) {
            if (is_string(static::$aliases[$root])) {
                return $pos === false ? static::$aliases[$root] : static::$aliases[$root] . substr($alias, $pos);
            }
            foreach (static::$aliases[$root] as $name => $path) {
                if (strpos($alias . '/', $name . '/') === 0) {
                    return $path . substr($alias, strlen($name));
                }
            }
        }
        return false;
    }
}

spl_autoload_register(['App', 'autoload'], true, true);
$config = [
    'db' => [
        'dns' => 'mysql:host=194.247.12.107;dbname=test_db',
        'username' => 'test_db',
        'password' => 'q6Qlr7vF',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ],

    ],
];
App::$app = new \app\components\Application($config);
App::$app->start();